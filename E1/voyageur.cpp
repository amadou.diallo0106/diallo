#include <iostream>
#include <string>
#include "Voyageur.h"
using namespace std;

//Definition des constructeurs par defaut et à deux arguments
Voyageur::Voyageur() : nom("VoyageurX"), age(20), categorie("Adulte") {}
Voyageur::Voyageur(string voyageur, int x, string cat) : nom(voyageur), age(x), categorie(cat) {}

//Definition des accesseurs
string Voyageur::getNom() { return this->nom; }
int Voyageur::getAge() { return this->age; }
string Voyageur::getCategorie() { return this->categorie; }
//Definition des modificateurs
void Voyageur::setNom(string nom) { this->nom = nom; }
void Voyageur::setAge(int age) 
{ 
    this->age = age;
    if (age <= 1)
    {
        this->categorie = "Nourrison";
    }
    else if (age > 1 && age < 17)
    {
        this->categorie = "Enfant";
    }
    else if (age > 18 && age < 60)
    {
        this->categorie = "Adulte";
    }
    else
    {
        this->categorie = "Senior";
    }

}
//Definition methode d'affichage
void Voyageur::afficherDonnees()
{
    cout << "Voyageur : " << this->getNom() << endl;
    cout << "Age: " << this->getAge() << endl;
    cout << "Categorie: " << this->getCategorie() << endl;
}

int main()
{
    //Initialisation du premier objet avec constructeur par défaut
    Voyageur Voyageur1;
    Voyageur1.afficherDonnees();
    //Initialisation d'objet avec paramètres par l'utilisateur
    string nom;
    int age;
    do
    {
        cout << "Quel est le nom du voyageur? ";
        getline(cin, nom);
    } while (nom.length() < 2);

    do
    {
        cout << "Quelle est son age? ";
        cin >> age;
    } while (age < 0);

    Voyageur Voyageur2;
    Voyageur2.setNom(nom);
    Voyageur2.setAge(age);
    Voyageur2.afficherDonnees();

    return 0;
}